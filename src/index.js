/**
 * KaTeX/Math plugin for EditorJS.
 *
 * @author Valeriu Paloş (hi@vpalos.com)
 * @license The ISC License (ISC)
 */

import "./index.css";
import katex from "katex";
import "katex/contrib/mhchem/mhchem";
import Icon from "./icon.svg";

export default class EditorJSKatex {
    constructor({ data, config, api, readOnly }) {
        this.api = api;
        this.readOnly = readOnly;

        this.config = {
            placeholder: config.placeholder || "",
            saveHtml: config.saveHtml || false,
        };

        this.data = {
            latex: data.latex || "x^2",
        };

        this.nodes = {
            wrapper: null,
            canvas: null,
            input: null,
        };

        this.css = {
            wrapper: [this.api.styles.block, "cdx-katex"],
            canvas: "cdx-katex__canvas",
            input: [this.api.styles.input, "cdx-katex__input"],
            error: "cdx-katex--error",
        };
    }

    static get isReadOnlySupported() {
        return true;
    }

    render() {
        this.nodes.wrapper = this.create("div", this.css.wrapper);
        this.nodes.canvas = this.create("div", this.css.canvas);
        this.nodes.input = this.create("textarea", this.css.input, {
            placeholder: this.config.placeholder,
            readonly: this.readOnly,
            value: this.data.latex,
        });

        this.nodes.wrapper.appendChild(this.nodes.canvas);
        this.nodes.wrapper.appendChild(this.nodes.input);

        this.nodes.input.addEventListener("keyup", (event) => this.update(event.target.value));

        this.update(this.data.latex);

        return this.nodes.wrapper;
    }

    create(tagName, classNames = null, attributes = {}) {
        const el = document.createElement(tagName);

        if (Array.isArray(classNames)) {
            el.classList.add(...classNames);
        } else if (classNames) {
            el.classList.add(classNames);
        }

        for (const attrName in attributes) {
            el[attrName] = attributes[attrName];
        }

        return el;
    }

    update(latex) {
        try {
            this.data = {
                latex,
                html: katex.renderToString(latex, {
                    output: "html",
                    throwOnError: true,
                    displayMode: true,
                }),
            };
            this.nodes.canvas.innerHTML = this.data.html;
            this.error = null;
        } catch (e) {
            if (e instanceof katex.ParseError) {
                this.error = e.message;
            } else {
                throw e;
            }
        }
    }

    get error() {
        return this._error;
    }

    set error(message) {
        if (message) {
            console.error(message);
            this._error = message;
            this.nodes.canvas.classList.add(this.css.error);
            if (!this.data.html) {
                this.nodes.canvas.innerHTML = this.data.html = message;
            }
        } else {
            if (this._error) {
                this.nodes.canvas.classList.remove(this.css.error);
            }
        }
    }

    validate(data) {
        return !!data.latex.trim();
    }

    save(content) {
        const result = {
            latex: this.data.latex,
        };

        if (this.config.saveHtml) {
            result.html = this.data.html;
        }

        return result;
    }

    onPaste(event) {
        this.data = {
            latex: event.detail.data.innerHTML,
        };
    }

    static get conversionConfig() {
        return {
            export: "latex",
            import: "latex",
        };
    }

    static get sanitize() {
        return {
            latex: true,
            html: true,
        };
    }

    // static get pasteConfig() {
    //     return {
    //         tags: ["P"],
    //     };
    // }

    static get toolbox() {
        return {
            icon: Icon,
            title: "LaTeX",
        };
    }
}
