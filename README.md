# LaTeX/Math plugin for EditorJS

LaTeX/Math plugin for [EditorJS](https://editorjs.io/) using the [KaTeX](https://katex.org/) library for rendering.

## Usage

Add a new Tool to the `tools` property of the Editor.js initial config.

```js
import EditorJSKatex from "./editorjs-katex/index.js";

const editor = EditorJS({
    tools: {
        katex: {
            class: EditorJSKatex,
            shortcut: "CMD+SHIFT+X",
        },
    },
});
```

> Loading the bundled version of the library in your HTML (e.g. `<script src=".../dist/bundle.js"></script>`), will make `window.EditorJSKatex` available globally.

## Configuration

Parameters:

-   `placeholder = "..."`: placeholder value for the input field (default: '');
-   `saveHtml = true|false`: if `true`, the `data.html` field is added to the output with the rendered HTML;

## Output data

| Field | Type     | Description |
| ----- | -------- | ----------- |
| latex | `string` | KaTeX code  |

```json
{
    "type": "katex",
    "data": {
        "latex": "...",
        "html": "..." // only if config.saveHtml == true
    }
}
```

## Credits

-   Based on [KaTeX](https://katex.org/) for rendering.
